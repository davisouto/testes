<?php
    /**
     * Exemplo de select pegando valores do banco de dados
     *
     * @author Davi Souto
     *         05/05/2016
     */
     
     // Estou simulando esse array como o resultado que vem do banco, mas na sua aplicação
     // você substitua pelo resultado do banco
     $resultado = array(
         0  =>  array(
             "id"   =>  1,
             "nome" =>  "Davi Souto"
         ),
         1  =>  array(
             "id"   =>  2,
             "nome" =>  "Emerson Souto"
         ),
         2  =>  array(
             "id"   =>  3,
             "nome" =>  "Ana Beatriz"
         ),
         3  =>  array(
             "id"   =>  10,
             "nome" =>  "Vivian"
         ),
         4  =>  array(
             "id"   =>  20,
             "nome" =>  "Zézinho da Silva"
         ),
         5  =>  array(
             "id"   =>  25,
             "nome" =>  "Fulano"
         ),
         6  =>  array(
             "id"   =>  99,
             "nome" =>  "Oceano Pacifico"
         ),
     )
     

?>

<html>
    <head>
        <meta charset="UTF-8" />
        <title>Exemplo Foreach</title>
    </head>
    <body>
        <h1>Exemplo Foreach</h1>
        <p>Observe o resultado da página usando a opção de exibir código fonte do navegador</p>
        <select id="meu_select">
            <option value="">Selecione uma opcao</option>

                <!-- Para rodar um código PHP deve-se colocar entre as chaves <?php ?> -->
                <?php
                    // Aqui iniciamos o loop no foreach percorrendo o resultado do banco de dados
                    // Nesse exemplo estou usando a variavel $resultado percorrendo como um array,
                    // mas se o seu for um objeto pode percorrer usando a função fetch() dentro de um while
                    // O foreach vai fazer com que cada índice do array $resultado seja percorrido e lido pela
                    // variavel $linhai
                    // Detalhe: Você pode abrir chave "{" para abrir o foreach e fechar com outra chave "}", mas por
                    // questão visual eu prefiro usar o ":" para abrir e fechar com "endforeach" quando uso foreach
                    // junto ao html
                    ///
                    foreach($resultado as $linha):
                ?>
                
                    <!-- 
                        Tudo que estiver aqui ira fazer parte do foreach, inclusive esse comentário rs 
                        O option abaixo será escrito da seguinte forma:
                        
                        <option value="(ID DO USUÁRIO)">NOME DO USUÁRIO</option>
                    -->
                    <option value="<?php echo $linha['id']; ?>"><?php echo $linha['nome']; ?></option>
                    
                <?php endforeach; // Aqui fechamos o loop ?>

            </select>
    </body>
</html>
